﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;



public class StateObject
{
	// Client  socket.  
	public Socket workSocket = null;
	// Size of receive buffer.  
	public const int BufferSize = 100;
	// Receive buffer.  
	public byte[] buffer = new byte[BufferSize];
	// Received data string.  
	public string sbuffer;
}


public class AsyncServer : MonoBehaviour
{
	// Thread signal.  
	public static ManualResetEvent allDone = new ManualResetEvent(false);

	static Socket listener;


	public void StartListening(string string_ip, int port)
	{

		// Establish the remote endpoint for the socket.
		IPAddress ipAddress;
		IPAddress.TryParse(string_ip, out ipAddress);

		IPEndPoint localEndPoint = new IPEndPoint(ipAddress, port);

		
		// Create a TCP/IP socket.  
		listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);  //tcp

		//var listener2 = new Socket(ipAddress.AddressFamily, SocketType.Dgram, ProtocolType.Udp); //udp


		// Bind the socket to the local endpoint and listen for incoming connections.  
		try
		{
			listener.Bind(localEndPoint);
			listener.Listen(100);

			// Set the event to nonsignaled state.  
			//allDone.Reset();

			// Start an asynchronous socket to listen for connections.  
			Debug.Log("Waiting for a connection...");

			listener.BeginAccept( new AsyncCallback(AcceptCallback), listener );
		}
		catch (Exception e)
		{
			Debug.Log("Error in StartListening: " + e.ToString());
		}
	}


	public void StopListening() {

		if (listener != null) {
			listener.Shutdown(SocketShutdown.Both);
			listener.Close();
		}
		
	}


	private void AcceptCallback(IAsyncResult ar)
	{
		// Signal the main thread to continue.  
		allDone.Set();

		// Get the socket that handles the client request.  
		Socket listener = (Socket)ar.AsyncState;
		Socket handler = listener.EndAccept(ar);

		// Create the state object.  
		StateObject state = new StateObject();
		state.workSocket = handler;

		handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
			new AsyncCallback(ReadCallback), state);
	}


	private void ReadCallback(IAsyncResult ar)
	{
		// Retrieve the state object and the handler socket  
		// from the asynchronous state object.  
		StateObject state = (StateObject)ar.AsyncState;
		Socket handler = state.workSocket;

		// Read data from the client socket.   
		int bytesRead = handler.EndReceive(ar);

		if (bytesRead > 0)
		{
			// There  might be more data, so store the data received so far.  
			state.sbuffer += Encoding.ASCII.GetString( state.buffer, 0, bytesRead );

			int eof_index = state.sbuffer.IndexOf("<EOF>");
			if (eof_index > -1)
			{
				Debug.Log(state.sbuffer.Substring(0, eof_index));

				state.sbuffer = "";
			}

			//process data recieve
			handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);

			
		}

		allDone.Set();
	}


	private void OnDestroy()
	{
		StopListening();
	}

}