﻿using UnityEngine;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;



public class AsyncClient : MonoBehaviour
{
	//ManualResetEvent instances signal completion.
	ManualResetEvent connectDone = new ManualResetEvent(false);
	ManualResetEvent sendDone = new ManualResetEvent(false);
	ManualResetEvent receiveDone = new ManualResetEvent(false);

	static Socket client;

	public bool Connected {
		get {
			if (client != null) {
				return client.Connected;
			}
			
			return false;
		}
	}


	public void StartClient(string string_ip, int port)
	{
		// Connect to a remote device.
		try
		{
			// Establish the remote endpoint for the socket.
			IPAddress ipAddress;
			IPAddress.TryParse(string_ip, out ipAddress);

			IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

			// Create a TCP/IP socket.
			client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

			// Connect to the remote endpoint.
			client.BeginConnect(remoteEP,
				new AsyncCallback(ConnectCallback), client);
			
			connectDone.WaitOne();
		}
		catch (Exception e)
		{
			Debug.Log("Error in StartClient: " + e.ToString());
		}
	}


	public void StopClient() {
		if (client != null) {
			client.Shutdown(SocketShutdown.Both);
			client.Close();
		}
	}


	private void ConnectCallback(IAsyncResult ar)
	{
		try
		{
			// Retrieve the socket from the state object.
			Socket client = (Socket)ar.AsyncState;

			// Complete the connection.
			client.EndConnect(ar);

			Debug.Log("Connection established!");
			
			// Signal that the connection has been made.
			connectDone.Set();
		}
		catch (Exception e)
		{
			Debug.Log("Error in ConnectCallback: " + e.ToString());
		}
	}
	

	public void Send(string data)
	{
		data += "<EOF>";

		// Convert the string data to byte data using ASCII encoding.
		byte[] byteData = Encoding.ASCII.GetBytes(data);

		// Begin sending the data to the remote device.
		client.BeginSend(byteData, 0, byteData.Length, 0,
			new AsyncCallback(SendCallback), client);
	}


	private void SendCallback(IAsyncResult ar)
	{
		try
		{
			// Retrieve the socket from the state object.
			Socket client = (Socket)ar.AsyncState;

			// Complete sending the data to the remote device.
			int bytesSent = client.EndSend(ar);
			Debug.Log( string.Format( "Sent {0} bytes to server.", bytesSent) );

			// Signal that all bytes have been sent.
			sendDone.Set();
		}
		catch (Exception e)
		{
			Debug.Log("Error in SendCallback: " + e.ToString());
		}
	}


	private void OnDestroy()
	{
		StopClient();
	}

}