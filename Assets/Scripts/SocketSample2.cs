﻿using System.Collections;
using UnityEngine;


public class SocketSample2 : MonoBehaviour {

	public string ServerIp;
	public int ServerPort;


	AsyncClient client;
	AsyncServer server;


	public void StartServer() {

		server.StartListening(ServerIp, ServerPort);

		//turn off canvas
		GameObject.FindGameObjectWithTag("ui").SetActive(false);
	}


	IEnumerator SendSeveralTimes() {

		//wait until connection is made
		while (!client.Connected)
			yield return null;

		//send data 10 times
		int count = 10;

		for (int i = 0; i < count; i++)
		{
			client.Send("Send time: " + i.ToString());
			yield return new WaitForSeconds(1f);
		}
	}


	public void StartClient()
	{
		Debug.Log("Start client");

		client.StartClient(ServerIp, ServerPort);

		StartCoroutine(SendSeveralTimes());

		//turn off canvas
		GameObject.FindGameObjectWithTag("ui").SetActive(false);
	}


	private void Start()
	{
		client = GetComponent< AsyncClient >();
		server = GetComponent< AsyncServer >();
	}



}
